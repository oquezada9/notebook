﻿import { Injectable, Inject } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
@Injectable()
export class WheatherForecastService {

    constructor(public http: Http) {

    }

    getWeatherForecasts() {
        return this.http.get('http://localhost:5001/api/WeatherForecasts/Get')
            .map(result => result);
    }
    
    getSummaries() {
        return this.http.get('http://localhost:5001/api/WeatherForecasts/GetSummaries')
            .map(res => res);
    }
}