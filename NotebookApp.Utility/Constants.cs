﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NotebookApp.Utility
{
    public static class Constants
    {
        public static class Database
        {
            public const string AppSettingConnectionString = "MongoConnection:ConnectionString";
            public const string AppSettingName = "MongoConnection:Database";
            public const string ConnectionString = "mongodb://admin:abc123!@localhost";
            public const string Name = "NotesDb";
        }

        public static class Swagger
        {
            public const string EndPoint = "/swagger/v1/swagger.json";
        }

        public static class API
        {
            public const string Name = "Notebook";
            public const string Version = "v1";
            public const string Schema = "Bearer";
            public const string IISUrl = "http://localhost:5001";
            public const string CorsAllowAny = "CorsAllowAnyPolicyName";
        }

        public static class OAuth
        {
            public const string IISUrl = "http://localhost:5000";
        }

    }
}
