using NotebookApp.Data.Models;
using NotebookApp.Repo.Models;
using NotebookApp.Repo.Repository;
using NotebookApp.Service.Interfaces;
using NotebookApp.Utility;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using NUnit.Framework;
using Moq;

namespace NotebookApp.Service.UnitTests
{
    [TestFixture]
    public class NoteServiceTests
    {
        Mock<NoteRepository> _mockNoteRepository;
        INoteService _noteService;

        [SetUp]
        public void LoadContext()
        {
            _mockNoteRepository = new Mock<NoteRepository>(new Settings
                {
                    ConnectionString = Constants.Database.ConnectionString,
                    Database = Constants.Database.Name
                });
            _noteService = new NoteService(_mockNoteRepository.Object);
        }

        [Test]
        public async Task Get_All_ReturnsList()
        {
            var notes = await _noteService.Get();
            Assert.IsTrue(notes.Any());
        }
    }
}
