﻿using NotebookApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NotebookApp.Service.Interfaces
{
    public interface INoteService
    {
        Task<IEnumerable<Note>> Get();
        Task<Note> Get(string id);
        void Post(string value);
        void Put(string id, string value);
        void Delete(string id);
    }
}
