﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NotebookApp.Service.Interfaces
{
    public interface ISystemService
    {
        string Get(string setting);  
    }
}
