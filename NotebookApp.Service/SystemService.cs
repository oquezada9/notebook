﻿using NotebookApp.Data.Models;
using NotebookApp.Repo.Repository;
using NotebookApp.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace NotebookApp.Service
{
    public class SystemService : ISystemService
    {
        private readonly INoteRepository _noteRepository;

        public SystemService(INoteRepository noteRepository)
        {
            _noteRepository = noteRepository;
        }

        public string Get(string setting)
        {
            if (setting == "init")
            {
                _noteRepository.RemoveAllNotes();
                _noteRepository.AddNote(new Note()
                {
                    Id = "1",
                    Body = "Test note 1",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    UserId = 1
                });
                _noteRepository.AddNote(new Note()
                {
                    Id = "2",
                    Body = "Test note 2",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    UserId = 1
                });
                _noteRepository.AddNote(new Note()
                {
                    Id = "3",
                    Body = "Test note 3",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    UserId = 2
                });
                _noteRepository.AddNote(new Note()
                {
                    Id = "4",
                    Body = "Test note 4",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    UserId = 2
                });

                return "Done";
            }

            return "Unknown";
        }
    }
}
