﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NotebookApp.Data.Models;
using NotebookApp.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotebookApp.Api.Controllers
{
    [Route("api/[controller]")]
    public class NotesController : Controller
    {
        private readonly INoteService _noteService;

        public NotesController(INoteService noteService)
        {
            _noteService = noteService;
        }

        // GET: notes/notes
        [HttpGet]
        public Task<string> Get()
        {
            return GetNoteInternal();
        }

        [HttpGet]
        private async Task<string> GetNoteInternal()
        {
            var notes = await _noteService.Get();
            return JsonConvert.SerializeObject(notes);
        }

        // GET api/notes/5
        [HttpGet("{id}")]
        public Task<string> Get(string id)
        {
            return GetNoteByIdInternal(id);
        }

        [HttpGet]
        private async Task<string> GetNoteByIdInternal(string id)
        {
            var note = await _noteService.Get(id) ?? new Note();
            return JsonConvert.SerializeObject(note);
        }

        // POST api/notes
        [HttpPost]
        public void Post([FromBody]string value)
        {
            _noteService.Post(value);
        }

        // PUT api/notes/5
        [HttpPut("{id}")]
        public void Put(string id, [FromBody]string value)
        {
            _noteService.Put(id, value);
        }

        // DELETE api/notes/5
        [HttpDelete]
        public void Delete(string id)
        {
            _noteService.Delete(id);
        }
    }
}
