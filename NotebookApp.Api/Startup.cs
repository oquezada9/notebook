﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NotebookApp.Repo.Models;
using NotebookApp.Repo.Repository;
using NotebookApp.Utility;
using Swashbuckle.AspNetCore.Swagger;
using IdentityServer4.AccessTokenValidation;
using NotebookApp.Service.Interfaces;
using NotebookApp.Service;

namespace NotebookApp.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            services.AddCors(o => o.AddPolicy(Constants.API.CorsAllowAny, builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));

            services.AddMvcCore()
                .AddAuthorization()
                .AddJsonFormatters();

            services.AddAuthentication(Constants.API.Schema)
                .AddIdentityServerAuthentication(options =>
                {
                    options.Authority = Constants.OAuth.IISUrl;
                    options.RequireHttpsMetadata = false;

                    options.ApiName = Constants.API.Name;
                });
            DIContiner(services);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(Constants.API.Version, new Info { Title = Constants.API.Name, Version = Constants.API.Version });
            });
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(Constants.API.CorsAllowAny);

            app.UseAuthentication();
            app.UseStaticFiles();
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint(Constants.Swagger.EndPoint, Constants.API.Name);
            });

            app.UseMvc();
        }

        private void DIContiner(IServiceCollection services)
        {
            services.AddScoped<INoteRepository, NoteRepository>();
            services.AddScoped<Settings>(s =>
                                new Settings
                                {
                                    ConnectionString = Configuration.GetSection(Constants.Database.AppSettingConnectionString).Value,
                                    Database = Configuration.GetSection(Constants.Database.AppSettingName).Value
                                });
            services.AddTransient<INoteService, NoteService>();
            services.AddTransient<ISystemService, SystemService>();
        }
    }
}
