﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using NotebookApp.Utility;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace NotebookApp.OAuth
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseUrls(Constants.OAuth.IISUrl)
                .UseStartup<Startup>()
                .Build();
    }
}
