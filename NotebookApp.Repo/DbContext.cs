﻿using MongoDB.Driver;
using NotebookApp.Repo.Models;
using NotebookApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NotebookApp.Utility;

namespace NotebookApp.Repo
{
    public class DbContext
    {
        private readonly IMongoDatabase _database = null;

        public DbContext(Settings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            if (client != null)
                _database = client.GetDatabase(settings.Database);
        }

        public IMongoCollection<Note> Notes
        {
            get
            {
                return _database.GetCollection<Note>("Note");
            }
        }
    }
}
